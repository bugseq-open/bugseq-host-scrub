#!/bin/bash

# meant to be run in mambaorg/micromamba docker image

set -euxo pipefail

micromamba create --quiet -y -f environment.yml

set +x
eval "$(micromamba shell hook --shell=bash)"
micromamba activate bugseq_host_scrub
set -x

pytest

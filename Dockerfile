FROM mambaorg/micromamba:1.1.0

COPY --chown=$MAMBA_USER:$MAMBA_USER environment.yml /tmp/environment.yml
RUN micromamba create --quiet -y -f /tmp/environment.yml && \
    micromamba clean -ay

# ENV_NAME defined in environment.yml defaults micromamba to activate that env
ENV ENV_NAME=bugseq_host_scrub
COPY bugseq_host_scrub/ /bugseq_host_scrub

# user change needed to ln -s
USER root
RUN ln -s /bugseq_host_scrub/bugseq_host_scrub /usr/local/bin/bugseq_host_scrub
USER $MAMBA_USER

# Set up defaults
ARG MAMBA_DOCKERFILE_ACTIVATE=1
RUN bugseq_host_scrub prepare_index -o /bugseq_host_scrub/index.mmi
ENTRYPOINT ["/usr/local/bin/_entrypoint.sh", "bugseq_host_scrub", "filter_dir", "-r", "/bugseq_host_scrub/index.mmi"]

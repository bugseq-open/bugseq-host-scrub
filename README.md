# BugSeq Human Scrub

## Dependencies
See [`environment.yml`](./environment.yml)

## Usage

Note: currently only functional for nanopore sequencing data.

### Single File
```shell
./bugseq_host_scrub filter_file \
    --input input.fastq.gz \
    --output output.fastq.gz \
    --threads 8
```

### Many files in a directory
```shell
./bugseq_host_scrub filter_dir \
    --input_dir folder_containing_fastqs \
    --output_dir folder_for_filtered_fastqs \ # Preserves input_dir directory tree inside output_dir
    --threads 8
```

### Storing the index for future use
```shell
./bugseq_host_scrub prepare_index \
    --output human_filter.mmi \
    --threads 8

./bugseq_host_scrub filter_file \
    --reference human_filter.mmi \
    --input input.fastq.gz \
    --output output.fastq.gz \
    --threads 8
```

### Custom Host Organism
```shell
./bugseq_host_scrub filter_file \
    --input input.fastq.gz \
    --output output.fastq.gz \
    --reference Gallus_gallus-reference.fna.gz \
    --threads 8
```

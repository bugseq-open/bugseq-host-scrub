import argparse
import logging
import multiprocessing
import os
import socket
import subprocess
import sys
import tempfile
import time
from pathlib import Path

from filter_fastq_dir import filter_fastq_dir
from filter_host import filter_host_cmd
from prepare_index import prepare_index


def _log_host_config(log_file):
    # https://stackoverflow.com/a/28161352
    mem_bytes = os.sysconf("SC_PAGE_SIZE") * os.sysconf("SC_PHYS_PAGES")
    mem_gib = mem_bytes / (1024.0**3)
    with tempfile.TemporaryDirectory() as tmp:
        sample_temp_dir = tmp
    lsblk_output = subprocess.run(
        ["lsblk", "-f"], capture_output=True, check=True
    ).stdout.decode()

    logging.info(
        """
Args: %(args)s
Log file: %(log_file)s
Hostname: %(hostname)s
CPUs: %(cpu_count)d
Memory: %(memory)f
Sample temp dir: %(tempdir)s
lsblk: %(lsblk_output)s
                 """,
        {
            "args": sys.argv,
            "log_file": log_file,
            "hostname": socket.gethostname(),
            "cpu_count": multiprocessing.cpu_count(),
            "memory": mem_gib,
            "tempdir": sample_temp_dir,
            "lsblk_output": lsblk_output,
        },
    )


def cli():
    # create the top-level parser
    parser = argparse.ArgumentParser()
    # Default to help: https://stackoverflow.com/a/61680800
    parser.set_defaults(func=lambda args: parser.print_help())

    parser.add_argument(
        "--log_file",
        required=False,
        default=tempfile.NamedTemporaryFile(
            "w",
            prefix=f"bugseq-host-scrub-{time.strftime('%Y%m%d-%H%M%S')}-",
            delete=False,
        ).name,
        help="Log file to write to",
    )

    parser.add_argument(
        "--log_level",
        required=False,
        default="INFO",
        choices=logging._nameToLevel.keys(),  # https://stackoverflow.com/a/65534541
        help="Log level",
    )

    subparsers = parser.add_subparsers()

    parser_prepare_index = subparsers.add_parser(
        "prepare_index", help="Download and index the human genome+transcriptome."
    )
    parser_prepare_index.add_argument(
        "-o",
        "--output",
        type=Path,
        required=True,
        help="Output path to store human index.",
    )
    parser_prepare_index.add_argument(
        "-t", "--threads", type=int, default=multiprocessing.cpu_count()
    )
    parser_prepare_index.set_defaults(
        func=lambda args: prepare_index(args.output, args.threads)
    )

    reference_help = (
        "FASTA file (can be gzip compressed) or minimap2 index of reference "
        "(eg. human genome/transcriptome) to filter against. "
        "If left empty, this script will download the human genome "
        "and transcriptome to filter against - Internet connection required. "
        "To reuse an index, see prepare_index subcommand."
    )

    # File filtering
    parser_filter_file = subparsers.add_parser(
        "filter_file", help="Filter a single FASTQ file (may be gzip compressed)"
    )
    parser_filter_file.add_argument("-i", "--input", type=Path, required=True)
    parser_filter_file.add_argument("-r", "--reference", type=Path, help=reference_help)
    parser_filter_file.add_argument("-o", "--output", type=Path, required=True)
    parser_filter_file.add_argument(
        "-t", "--threads", type=int, default=multiprocessing.cpu_count()
    )
    parser_filter_file.set_defaults(
        func=lambda args: filter_host_cmd(
            args.input, args.reference, args.output, args.threads
        )
    )

    # Directory parsing
    parser_filter_dir = subparsers.add_parser(
        "filter_dir",
        help=(
            "Filter an entire directory (recursively) "
            "containing FASTQs (may be gzip compressed)."
        ),
    )
    parser_filter_dir.add_argument(
        "-i",
        "--input_dir",
        type=Path,
        required=True,
        help=(
            "Searches for files in DIRECTORY with extensions "
            ".fastq, .fastq.gz, .fq and .fq.gz and performs filtering on them."
        ),
    )
    parser_filter_dir.add_argument("-r", "--reference", type=Path, help=reference_help)
    parser_filter_dir.add_argument(
        "-o",
        "--output_dir",
        type=Path,
        required=True,
        help=(
            "All files from INPUT_DIR are filtered and compressed (if not already) "
            "into DIRECTORY, preserving directory tree."
        ),
    )
    parser_filter_dir.add_argument(
        "-t", "--threads", type=int, default=multiprocessing.cpu_count()
    )
    parser_filter_dir.set_defaults(
        func=lambda args: filter_fastq_dir(
            args.input_dir, args.reference, args.output_dir, args.threads
        )
    )

    # parse the args and call whatever function was selected
    args = parser.parse_args()

    logging.basicConfig(
        level=args.log_level,
        format="%(asctime)s [%(threadName)s] [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler(args.log_file),
            logging.StreamHandler(),
        ],
    )

    _log_host_config(args.log_file)

    args.func(args)

import io
import logging
import tempfile
from unittest.mock import mock_open, patch

from utils import download_file, download_human, subprocess_run


@patch("urllib.request.urlopen", new_callable=mock_open, read_data=b"foobar")
def test_download_file(mock_urlopen):
    dest = tempfile.NamedTemporaryFile()
    download_file("https://invalidfiles.com", dest.name)

    mock_urlopen.assert_called_once_with("https://invalidfiles.com")
    with open(dest.name, "r") as f:
        assert f.read() == "foobar"


@patch("utils.download_file")
def test_download_human(mock_download_file):
    download_human("invalid")
    mock_download_file.assert_called_once_with(
        "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/009/914/755/"
        "GCA_009914755.4_T2T-CHM13v2.0/"
        "GCA_009914755.4_T2T-CHM13v2.0_genomic.fna.gz",
        "invalid",
    )


def test_subprocess_run_success():
    logger = logging.getLogger(__name__)
    buffer = io.StringIO()
    logger.addHandler(logging.StreamHandler(buffer))
    logger.setLevel(logging.DEBUG)

    subprocess_run(["echo", "hello"], logger=logger, check=True)
    buffer.seek(0)
    assert "hello" in buffer.read()

from collections import OrderedDict, abc
from pathlib import Path
from typing import TextIO


class FileCache:
    def __init__(self, capacity: int, opener: abc.Callable[[Path], TextIO] = None):
        self._cache = OrderedDict()
        self._capacity = capacity
        if opener is None:
            self._opener = open
        else:
            self._opener = opener

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def open(self, file: Path):
        name = str(file)

        if name in self._cache:
            self._cache.move_to_end(name)
            return self._cache[name]

        self._cache[name] = self._opener(file)
        self._cache.move_to_end(name)

        if len(self._cache) > self._capacity:
            _, popped = self._cache.popitem(last=False)
            popped.close()

        return self._cache[name]

    def close(self):
        for _, handle in self._cache.items():
            handle.close()

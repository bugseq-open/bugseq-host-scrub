import gzip
import shutil
import tempfile
from pathlib import Path
from unittest.mock import patch

import pytest
from filter_host import filter_host


def test_filter_host_reference_basic():
    infile = Path("bugseq_host_scrub/testdata/inputs/keep.fastq")
    reference = Path("bugseq_host_scrub/testdata/reference/reference.fasta")

    with tempfile.NamedTemporaryFile("w+") as output, open(
        "bugseq_host_scrub/testdata/outputs/expected.fastq", "r"
    ) as expected_file:
        filter_host(infile, reference, output.name, 1)
        output.seek(0)
        actual = output.read()
        expected = expected_file.read()
        assert (
            actual == expected
        ), f"actual contents differ from expected. actual: '{actual}', expected: '{expected}'"


@patch("filter_host.download_human")
def test_filter_host_reference_no_reference(mock_download_human):
    infile = Path("bugseq_host_scrub/testdata/inputs/keep.fastq")
    reference = Path("bugseq_host_scrub/testdata/reference/reference.fasta")

    # stub out downloading human so we don't need to download in tests
    def _mock_write_human(dest):
        with open(reference, "r") as reference_f, gzip.open(dest, "wt") as dest_f:
            shutil.copyfileobj(reference_f, dest_f)

    mock_download_human.side_effect = _mock_write_human

    with tempfile.NamedTemporaryFile("w+") as output, open(
        "bugseq_host_scrub/testdata/outputs/expected.fastq", "r"
    ) as expected_file:
        filter_host(infile, None, output.name, 1)
        output.seek(0)
        actual = output.read()
        expected = expected_file.read()
        mock_download_human.assert_called_once()
        assert (
            actual == expected
        ), f"actual contents differ from expected. actual: '{actual}', expected: '{expected}'"


def test_filter_host_reference_no_retained():
    infile = Path("bugseq_host_scrub/testdata/inputs/scrub.fastq")
    reference = Path("bugseq_host_scrub/testdata/reference/reference.fasta")
    with tempfile.NamedTemporaryFile("w+") as output, pytest.raises(
        ValueError
    ) as e_info:
        filter_host(infile, reference, output.name, 1)
        assert "no reads left after filtering." in str(e_info)

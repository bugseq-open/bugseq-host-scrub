import logging
import os
import shutil
import subprocess
import threading
import urllib.request

logger = logging.getLogger(__name__)


# https://stackoverflow.com/a/39217788
def download_file(url, file_name):
    with urllib.request.urlopen(url) as response, open(file_name, "wb") as out_file:
        shutil.copyfileobj(response, out_file)


# Setup functions
def download_human(output_location):
    logger.info("Downloading human reference for filtering against")
    # CHM13v2.0 is latest as of June 10, 2022
    # https://github.com/marbl/CHM13
    # Download Genbank version as it includes Y and mitochondrial
    download_file(
        (
            "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/009/914/755/"
            "GCA_009914755.4_T2T-CHM13v2.0/"
            "GCA_009914755.4_T2T-CHM13v2.0_genomic.fna.gz"
        ),
        output_location,
    )


# https://codereview.stackexchange.com/a/17959
class LogPipe(threading.Thread):
    def __init__(self, logger, level=logging.INFO):
        """Setup the object with a logger and a loglevel
        and start the thread
        """
        threading.Thread.__init__(self)
        self.daemon = False
        self.logger = logger
        self.level = level
        self.fdRead, self.fdWrite = os.pipe()
        self.pipeReader = os.fdopen(self.fdRead)
        self.start()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def fileno(self):
        """Return the write file descriptor of the pipe"""
        return self.fdWrite

    def run(self):
        """Run the thread, logging everything."""
        for line in iter(self.pipeReader.readline, ""):
            self.logger.log(self.level, line.strip("\n"))

        self.pipeReader.close()

    def close(self):
        """Close the write end of the pipe."""
        os.close(self.fdWrite)


def subprocess_run(args, logger, **kwargs):
    check = kwargs.pop("check", True)  # default to always checking
    with LogPipe(logger=logger) as logpipe:
        proc = subprocess.Popen(args, stdout=logpipe, stderr=logpipe, **kwargs)
        proc.wait()
        if check and proc.returncode != 0:
            raise subprocess.CalledProcessError(
                returncode=proc.returncode, cmd=" ".join(args)
            )

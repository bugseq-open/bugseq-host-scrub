import gzip
import tempfile
from pathlib import Path

from filter_fastq_dir import filter_fastq_dir


def test_filter_fastq_dir():
    input_dir = Path("bugseq_host_scrub/testdata/inputs")
    reference = Path("bugseq_host_scrub/testdata/reference/reference.fasta")

    with tempfile.TemporaryDirectory() as output_dir:
        filter_fastq_dir(input_dir, reference, Path(output_dir), 1)

        # we expect two files
        assert sorted(p.name for p in Path(output_dir).glob("*")) == [
            "keep.fastq.gz",
        ]
        with gzip.open(Path(output_dir) / "keep.fastq.gz") as keep, open(
            "bugseq_host_scrub/testdata/outputs/expected.fastq"
        ) as expected_f:
            assert keep.read().decode() == expected_f.read()

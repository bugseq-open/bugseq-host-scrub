from pathlib import Path
from unittest.mock import MagicMock, mock_open

from file_cache import FileCache


def mock_opener(path):
    return mock_open(mock=MagicMock(path=path))


def test_insert_order():
    with FileCache(capacity=2, opener=mock_opener) as cache:
        cache.open(Path("foo"))
        cache.open(Path("bar"))
        cache.open(Path("baz"))

        assert "foo" not in cache._cache  # foo would be expired
        assert cache._cache["bar"].path == Path("bar")
        assert cache._cache["baz"].path == Path("baz")


def test_get_order():
    with FileCache(capacity=2, opener=mock_opener) as cache:
        cache.open(Path("foo"))
        cache.open(Path("bar"))
        cache.open(Path("foo"))
        cache.open(Path("baz"))

        assert (
            "bar" not in cache._cache
        )  # because foo is accessed, bar gets shifted out
        assert cache._cache["foo"].path == Path("foo")
        assert cache._cache["baz"].path == Path("baz")


def test_closes():
    cached_mocks = []

    with FileCache(capacity=2, opener=mock_opener) as cache:
        cache.open(Path("foo"))
        foo_item = cache._cache["foo"]
        cached_mocks.append(
            cache.open(Path("bar")),
        )
        cached_mocks.append(
            cache.open(Path("baz")),
        )

        foo_item.close.assert_called_once_with()

    assert len(cached_mocks) == 2
    cached_mocks[0].close.assert_called_once_with()
    cached_mocks[1].close.assert_called_once_with()

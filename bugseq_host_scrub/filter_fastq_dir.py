import base64
import gzip
import logging
import tempfile
from collections import defaultdict
from itertools import chain
from pathlib import Path
from uuid import uuid4

import pyfastx
from filter_host import filter_host
from tqdm import tqdm

from file_cache import FileCache

logger = logging.getLogger(__name__)


def log_filtered_read_counts(read_counts_before_filtering, read_counts_after_filtering):
    for fastq in read_counts_before_filtering.keys():
        filtered_read_count = (
            read_counts_before_filtering[fastq] - read_counts_after_filtering[fastq]
        )
        filtered_percent = round(
            filtered_read_count * 100 / read_counts_before_filtering[fastq], 2
        )
        logger.info(
            "Filtered %d reads (%d%%) in %s",
            filtered_read_count,
            filtered_percent,
            fastq,
        )


def _open_gzip(path):
    path.parent.mkdir(parents=True, exist_ok=True)
    return gzip.open(path, "wt", compresslevel=1)


def split_merged_fastq(merged_filtered_fastq, output_dir, total_reads):
    found_reads = 0
    read_counts_after_filtering = defaultdict(lambda: 0)

    with FileCache(capacity=128, opener=_open_gzip) as file_cache:
        for title, sequence, quality in tqdm(
            pyfastx.Fastq(
                str(merged_filtered_fastq), build_index=False, full_name=True
            ),
            desc="Splitting FASTQs after filtering",
            unit=" Reads",
            total=total_reads,
        ):
            found_reads += 1
            rel_path, title = remove_path_from_header(title)
            output_fastq = output_dir / rel_path

            fastq_handle = file_cache.open(output_fastq)
            fastq_handle.write(f"@{title}\n{sequence}\n+\n{quality}\n")
            read_counts_after_filtering[rel_path] += 1

    assert (
        found_reads == total_reads
    ), f"Expected to split {total_reads} reads but only found {found_reads} reads."

    return read_counts_after_filtering


# If the folder contains both "test.fastq" and "test.fastq.gz",
# we'd write both to "test.fastq.gz" because we compress everything
def check_fastq_files_compressed_duplicates(fastq_files):
    fastq_files = [
        fastq if fastq.suffix == ".gz" else Path(f"{fastq}.gz") for fastq in fastq_files
    ]
    assert len(set(fastq_files)) == len(
        fastq_files
    ), "Input folder contains files with names that will collide when '.gz' extension gets added to all filenames."


# Add relative path before original header
def add_path_to_header(encoded_fastq_path, title):
    title = f"{uuid4()}:{encoded_fastq_path} {title}"
    return title


def remove_path_from_header(title):
    encoded_fastq_path, restored_title = title.split(" ", 1)
    # We encode the FASTQ path name in the FASTQ title before mapping
    # (see: add_path_to_header)
    # This gets the base64 encoded path and decodes it.
    # The script-generated UUID is discarded.
    rel_path = Path(base64.urlsafe_b64decode(encoded_fastq_path.split(":")[1]).decode())
    return rel_path, restored_title


def combine_fastqs_recursively(input_dir, merged_fastq):
    fastq_extensions = ("**/*.fastq", "**/*.fq", "**/*.fastq.gz", "**/*.fq.gz")
    fastq_files = list(
        chain.from_iterable(input_dir.glob(ext) for ext in fastq_extensions)
    )
    assert len(fastq_files) > 0, "No FASTQ files found in input directory."

    check_fastq_files_compressed_duplicates(fastq_files)

    with open(merged_fastq, "w") as merged_fastq_handle:
        read_counts_before_filtering = {}
        for fastq in tqdm(fastq_files, desc="Merging files", unit="Files", position=0):
            # Store all filenames with a gz extension as we will write them compressed
            rel_path = fastq.relative_to(input_dir)
            if fastq.suffix != ".gz":
                rel_path = rel_path.parent / (rel_path.name + ".gz")

            encoded_fastq_path = base64.urlsafe_b64encode(
                bytes(str(rel_path), "utf-8")
            ).decode()

            read_count = 0
            for title, sequence, quality in tqdm(
                pyfastx.Fastq(str(fastq), build_index=False, full_name=True),
                desc="Merging reads before filtering",
                unit=" Reads",
                position=1,
            ):
                title = add_path_to_header(encoded_fastq_path, title)
                read_count += 1
                merged_fastq_handle.write(f"@{title}\n{sequence}\n+\n{quality}\n")

            read_counts_before_filtering[rel_path] = read_count
    return read_counts_before_filtering


def filter_fastq_dir(input_dir, reference, output_dir, threads):
    with tempfile.TemporaryDirectory() as tmpdir_name:
        tmp_path = Path(tmpdir_name)
        merged_fastq = tmp_path / Path("merged.fastq")
        read_counts_before_filtering = combine_fastqs_recursively(
            input_dir, merged_fastq
        )

        merged_filtered_fastq = tmp_path / Path("merged_filtered.fastq")
        retained_read_count, total_read_count = filter_host(
            merged_fastq, reference, merged_filtered_fastq, threads
        )

        output_dir.mkdir(parents=True, exist_ok=True)
        read_counts_after_filtering = split_merged_fastq(
            merged_filtered_fastq, output_dir, retained_read_count
        )

        log_filtered_read_counts(
            read_counts_before_filtering, read_counts_after_filtering
        )

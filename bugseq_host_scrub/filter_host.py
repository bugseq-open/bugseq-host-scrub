import csv
import logging
import tempfile
from pathlib import Path

from utils import download_human, subprocess_run

logger = logging.getLogger(__name__)


# Filtering functions
def get_unmapped_reads(paf, reads_to_keep_file):
    retained_read_count = 0
    total_read_count = 0
    with open(paf, "r", newline="") as paf_handle, open(
        reads_to_keep_file, "w"
    ) as reads_to_keep_writer:
        paf_reader = csv.reader(paf_handle, delimiter="\t")
        for row in paf_reader:
            total_read_count += 1
            # Reference is "*" if unmapped in PAF format:
            # https://lh3.github.io/minimap2/minimap2.html
            if row[5] == "*":
                retained_read_count += 1
                reads_to_keep_writer.write(f"{row[0]}\n")
    return retained_read_count, total_read_count


def filter_host(infile, reference, output, threads):
    logger.info("Filtering reads...")
    with tempfile.TemporaryDirectory() as tmpdir_name:
        tmp_path = Path(tmpdir_name)
        if reference is None:
            reference = tmp_path / Path("human.fna.gz")
            download_human(reference)

        minimap2_output = tmp_path / Path("map.paf")
        subprocess_run(
            [
                "minimap2",
                "-x",
                "splice",
                "--paf-no-hit",
                "--split-prefix",
                str(tmp_path / Path("temp")),
                "--secondary=no",
                "-t",
                str(threads),
                "-o",
                str(minimap2_output),
                str(reference),
                str(infile),
            ],
            logger=logger,
            check=True,
        )

        reads_to_keep_file = tmp_path / Path("reads.txt")
        retained_read_count, total_read_count = get_unmapped_reads(
            minimap2_output, reads_to_keep_file
        )
        if retained_read_count == 0:
            raise ValueError(
                "Filtered all reads from input file(s); no reads left after filtering."
            )

        subprocess_run(
            [
                "seqkit",
                "grep",
                "--threads",
                str(threads),
                "-f",
                str(reads_to_keep_file),
                "-o",
                str(output),
                str(infile),
            ],
            logger=logger,
            check=True,
        )
    return retained_read_count, total_read_count


def filter_host_cmd(infile, reference, output, threads):
    retained_read_count, total_read_count = filter_host(
        infile, reference, output, threads
    )
    filtered_read_count = total_read_count - retained_read_count
    filtered_percent = round(filtered_read_count * 100 / total_read_count, 2)
    logger.info("Filtered %d reads (%f%%)", filtered_read_count, filtered_percent)

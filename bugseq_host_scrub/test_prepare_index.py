import tempfile
from unittest.mock import ANY, patch

from prepare_index import prepare_index


@patch("prepare_index.subprocess_run")
@patch("prepare_index.download_human")
def test_prepare_index(mock_download_human, mock_subprocess_run):
    with tempfile.TemporaryFile("w") as output:
        prepare_index(output.name, 1)
        mock_download_human.assert_called_once()
        mock_subprocess_run.assert_called_once_with(
            [
                "minimap2",
                "-x",
                "splice",
                "--split-prefix",
                "temp",
                "-I",
                "10G",
                "-t",
                "1",
                "-d",
                str(output.name),
                ANY,
            ],
            logger=ANY,
            check=True,
        )

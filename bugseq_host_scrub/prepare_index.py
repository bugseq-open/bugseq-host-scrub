import logging
import tempfile
from pathlib import Path

from utils import download_human, subprocess_run

logger = logging.getLogger(__name__)


def index_host_reference(fasta, minimap2_index, threads):
    subprocess_run(
        [
            "minimap2",
            "-x",
            "splice",
            "--split-prefix",
            "temp",
            "-I",
            "10G",
            "-t",
            str(threads),
            "-d",
            str(minimap2_index),
            str(fasta),
        ],
        logger=logger,
        check=True,
    )


def prepare_index(output, threads):
    with tempfile.TemporaryDirectory() as tmpdir_name:
        tmp_path = Path(tmpdir_name)
        human_fasta = tmp_path / Path("human.fna.gz")
        download_human(human_fasta)
        index_host_reference(human_fasta, output, threads)
